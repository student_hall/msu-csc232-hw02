/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Ryan Hall <ryan816@live.missouristate.edu>
 *
 *
 *
 * @brief   Entry point to this application.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <cstdlib>
#include <iostream>

/**
 * @brief Entry point to the computational version of the Ackermann Function
 * @param m first of the two indices to inserted into the Ackermann Function
 * @param n the second index that is used for evaluating the Ackermann Function
 * @return ackerVal holds the value after calculation of the Ackermann Function
 */
 int Acker(int m, int n);

/**
 * @brief Entry point to this application.
 * @remark You are encouraged to modify this file as you see fit to gain
 * practice in using objects.
 *
 * @param argc the number of command line arguments
 * @param argv an array of the command line arguments
 * @return EXIT_SUCCESS upon successful completion
 */
int main(int argc, char **argv) {

    std::cout << "Acker(1, 2) = " << Acker(1, 2)
              << std::endl;
    return EXIT_SUCCESS;
}

int Acker(int m, int n){
  int ackerVal;
  if(m == 0)
    ackerVal = n + 1;
  if(n == 0)
    ackerVal = Acker(m - 1, 1);
  if(m != 0 && n != 0)
    ackerVal = Acker(m - 1, Acker(m, n - 1));

return ackerVal;
}
